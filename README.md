# KramdownImageWithoutP

This is a simple Jekyll plugin, i make them to generate the img nodes without p wrapping nodes.

The original source was found in this ['github comment'](https://github.com/gettalong/kramdown/issues/98#issuecomment-495169800)

Special thanks to [@bouk](https://github.com/bouk) !

# Install
This gem it's only a Jekyll plugin.

Add this line in your Jekyll project Gemfile:

```gemfile
gem 'kramdown-image-without-p', git: 'https://gitlab.com/DelioDC/jekyll-plugin-kramdown-img-without-p.git'
```

Run bundle update:

```
bundle update
```

Check if installed with:

```
bundle info kramdown-image-without-p
```

Now **build your jekyll site**.

When the project was rebuild, check in the html of your post if the img is rendered as:
```html
<img ...>
```
If the plugin don't work the img node was wrapped inside p node like:
```html
<p>
    <img ...>
</p>
```

# Manual instalation

If you prefer test or use the plugin without gem, you can download the project file:
```
 lib/kramdown-image-without-p.rb 
```

Create a directory called ```_plugins``` inside your jekyll project and copy the .rb file inside them.

For more info about Jekyll plugins read the [Jekyll's official documentation](https://jekyllrb.com/docs/plugins/installation/).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
