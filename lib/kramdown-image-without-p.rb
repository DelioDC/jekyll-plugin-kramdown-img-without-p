# frozen_string_literal: true
require 'kramdown/converter/html'
##########################################################################################
# This simple plugin render the img nodes without a wrapping with a p tag. 
##########################################################################################
# Original source: https://github.com/gettalong/kramdown/issues/98#issuecomment-495169800
# Thanks! @bouk
##########################################################################################
module StandaloneImages
  def convert_p(el, indent)
    return super unless el.children.size == 1 && (el.children.first.type == :img || (el.children.first.type == :html_element && el.children.first.value == "img"))
    convert(el.children.first, indent)
  end
end

Kramdown::Converter::Html.prepend StandaloneImages
